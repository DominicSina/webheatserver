const server = require('../config/servers').server;
const controller = require('../controllers/controller')

server.get('/app', controller.serveApp)

server.get('/building', controller.serveBuilding)