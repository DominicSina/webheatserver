const server = require('../config/servers').server;
const socketController = require('../controllers/socketController');

server.ws('/temperature', socketController.onTemperatureConnection)

server.ws('/control', function(ws, req) {
    socketController.onControlConnection(ws, req)
    ws.on('message', function(msg){
        socketController.onControlMessage(msg,ws,req)
    })
})
