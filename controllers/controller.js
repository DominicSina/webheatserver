var fs = require('fs');
var path = require('path');
const base64url = require('base64url');

function serveApp(req, res){
    require('crypto').randomBytes(8, function (ex, buf) {
        var token = base64url(buf)

        res.render('tmp_export', { clientToken: token, hostAddress: require('../config/servers').externalAddress+":"+require('../config/servers').nodePort })
    });
}

function serveBuilding(req, res){
    res.setHeader('content-type','application/tscn')
    //fs.createReadStream('/home/dominicsina/Desktop/NodeProjects/HeatServer/public/DefaultHouse.tscn').pipe(res);
    fs.createReadStream(path.join(__dirname, '../public/DefaultHouse.tscn')).pipe(res);
}

exports.serveApp = serveApp
exports.serveBuilding = serveBuilding
