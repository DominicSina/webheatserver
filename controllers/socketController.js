var redisClient=require('../config/database').database
var createRedisHash=require('../config/database').createRedisHash
var createRedisTemperatureKey=require('../config/database').createRedisTemperatureKey
var TemperatureColor=require('../models/TemperatureColor').TemperatureColor

var tempSockets = {}

var roomlist=["G-699",
    "G-702", "G-703",
    "G-707", "G-708",
    "G-712", "G-713", "G-717", "G-718", "G-719",
    "G-724", "G-725", "G-726",
    "G-731", "G-732", "G-736", "G-737",
    "G-741", "G-742", "G-743", "G-748", "G-749",
    "G-750", "G-755", "G-756",
    "G-760", "G-761", "G-765", "G-766", "G-767",
    "G-772", "G-773", "G-774", "G-779",
    "G-780", "G-784", "G-785", "G-789",
    "G-790", "G-791", "G-796", "G-797", "G-798"]

var roomlist2=["Room0","Room1","Room2","Room3","Room4",
    "Room5","Room6","Room7","Room8", "Room9","Room10"]

function onTemperatureConnection(ws, req){
    console.log("Registering new temp socket:")
    console.log(req.query.clientToken)
    tempSockets[req.query.clientToken]=ws
}

function onControlConnection(ws, req){
    console.log("received new control socket connection")
    require('crypto').randomBytes(8, function (ex, buf) {
        var token = buf.toString('base64')
        var dict = {clientToken: token};
        var text = JSON.stringify(dict);

        console.log("Sent token to client")
    });
}

function onControlMessage(msg, ws, req){
    console.log("received control message from "+req.query.clientToken)
    console.log(msg);
    var messageData = JSON.parse(msg);

    if(messageData.dateTime == null){
        tempSockets[req.query.clientToken].isTimeSet=false
    }
    else{
        tempSockets[req.query.clientToken].isTimeSet=true

        var dateTime = new Date()
        dateTime.setTime(messageData.dateTime)

		let dataPoints = []
        for (var roomId=0; roomId <= 10; roomId = roomId + 1) {
            dataPoints.push({buildingId: messageData.buildingId, roomId: roomId, 
							dateTime: dateTime})
        }

        dataPoints.forEach(function(dataPoint){
			redisClient.ZRANGEBYSCORE(createRedisTemperatureKey(dataPoint), messageData.dateTime, messageData.dateTime, 
				function(err, reply){
		            if(err!=null){
		                console.log(error)
		            }
		            else if(reply!=null){
						if(reply.length>=1){
				            var tc=new TemperatureColor(parseInt(reply[0]))
				            var dict={"room":dataPoint.roomId.toString(), "temp":tc.temperature, "color": tc.color};
				            var text=JSON.stringify(dict);

				            tempSockets[req.query.clientToken].send(text)
						}
						else{
				            var dict={"room":dataPoint.roomId.toString(), "temp":-99, "color": {r:0.7,g:0.7,b:0.7,a:0.2}};
				            var text=JSON.stringify(dict);

				            tempSockets[req.query.clientToken].send(text)
						}
		            }
            	})
		})
    }
}

exports.onTemperatureConnection = onTemperatureConnection
exports.onControlConnection = onControlConnection
exports.onControlMessage = onControlMessage
