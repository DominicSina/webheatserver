var path = require('path');

const express = require('express');
const server = express();

server.set('view engine', 'pug')

server.set("views", path.join(__dirname, '../views'))

server.use(express.static(path.join(__dirname, '../public')))

var bindAddress
if(process.env.BIND_ADDRESS) {
    bindAddress=process.env.BIND_ADDRESS
}
else {
    bindAddress='0.0.0.0'
}

var externalAddress
if(process.env.EXTERNAL_ADDRESS) {
    externalAddress=process.env.EXTERNAL_ADDRESS
}
else {
    externalAddress='192.168.178.24'
    //externalAddress='127.0.0.1'
}

var nodePort
if(process.env.NODE_PORT) {
    nodePort=process.env.NODE_PORT
}
else {
    nodePort='3000'
}

const expressWs = require('express-ws')(server);

exports.server = server;
exports.socketServer = expressWs.getWss();
exports.nodePort = nodePort;
exports.bindAddress = bindAddress;
exports.externalAddress = externalAddress;

