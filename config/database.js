var config = require('../config/secret');

var options = {
    return_buffers: false,
    auth_pass: config.REDIS_PW
};

var redis = require("redis");
var redisClient
if(process.env.REDIS_ADDRESS) {
    //server.use(require('express-redis')(6379, '127.0.0.1', options, 'redis'));
    redisClient = redis.createClient({
        host: process.env.REDIS_ADDRESS,
        port: 6379,
        return_buffers: false,
        auth_pass: config.REDIS_PW
    });
}
else {
    redisClient = redis.createClient({
        //host: '51.15.228.16',
	host: '192.168.2.3',
        port: 6379,
        return_buffers: false,
        auth_pass: config.REDIS_PW
    });
}

var createRedisHash = function(obj){
    return "buildingId:"+obj.buildingId.toString()+"room:"+obj.room+"dateTime:"+obj.dateTime.getTime()
}

var createRedisTemperatureKey = function(obj){
    return "TemperaturesBuilding"+obj.buildingId.toString()+"Room"+obj.roomId.toString()
}

exports.database = redisClient;
exports.createRedisHash = createRedisHash;
exports.createRedisTemperatureKey = createRedisTemperatureKey;
