var redisClient=require('../config/database').database
var createRedisHash=require('../config/database').createRedisHash
var createRedisTemperatureKey=require('../config/database').createRedisTemperatureKey
const socketServer = require('./servers').socketServer;

const toad = require('mqttletoad');
const TemperatureColor=require('../models/TemperatureColor').TemperatureColor

var sendRoomTempToClients = function(temperature, roomId){
	socketServer.clients.forEach(function each(client) {
		if(client.upgradeReq.url.replace('.websocket', '').replace(/\?\S*/,'') == '/temperature/'){
			if(client.isTimeSet==null || !client.isTimeSet){
				var tc=new TemperatureColor(temperature)
				var dict={"room":roomId.toString(), "temp":tc.temperature, "color": tc.color};
				var text=JSON.stringify(dict);

				client.send(text);
			}
		}
	});
}

var storeRoomTempInDB = function(temperature, buildingId, roomId, dateTime){
	var datapoint =	{buildingId: buildingId, roomId: roomId, 
						dateTime: dateTime}
	
	
	var args = [ createRedisTemperatureKey(datapoint), dateTime.getTime(), dateTime.getTime()];
	redisClient.zcount(args, function (err, response) {
		if (err != null) {
			console.log(error)
		}
		else if(response<1){
			//ZADD TemperaturesBuildingXRoomY time temperature
			args = [ createRedisTemperatureKey(datapoint), dateTime.getTime(), temperature.toString()];
			redisClient.zadd(args, function (err, response) {
				if (err != null) {
					console.log(error)
				}
			})
		}
	})
	
};

var startMqtt = async function() {
    const client = await toad.connect('mqtt://163.172.165.52:1883')

    var mac_to_location = {
        "e3:dc:75:9c:04:6f":{roomId: 0, buildingId:0},
        "d9:60:eb:5f:df:e3":{roomId: 1, buildingId:0},
        "eb:10:8e:f0:e0:c3":{roomId: 2, buildingId:0},
        "0":{roomId: 0, buildingId:0},
        "1":{roomId: 1, buildingId:0},
        "2":{roomId: 2, buildingId:0},
        "3":{roomId: 3, buildingId:0},
        "4":{roomId: 4, buildingId:0},
        "5":{roomId: 5, buildingId:0},
        "6":{roomId: 6, buildingId:0},
        "7":{roomId: 7, buildingId:0},
        "8":{roomId: 8, buildingId:0},
        "9":{roomId: 9, buildingId:0},
        "10":{roomId: 10, buildingId:0}
    }

    await client.subscribe('+/Thingy Environment Service/Thingy Temperature Characteristic', (str, packet) => {
        var temperature

        var sensorMacAddr=packet.topic.split("/")[0]
        var location=mac_to_location[sensorMacAddr]

        if(sensorMacAddr!="e3:dc:75:9c:04:6f"&&sensorMacAddr!="d9:60:eb:5f:df:e3"&&sensorMacAddr!="eb:10:8e:f0:e0:c3"){
            var messageData = JSON.parse(str);
            temperature = messageData.temp.toString()
            temperature = parseFloat(str.replace(',','.'))

            var datapoint =	{buildingId: location.buildingId.toString(), room: "Room"+location.roomId.toString(), 
								dateTime: new Date(messageData.dateTime)}
            redisClient.HMSET(createRedisHash(datapoint),"temp", messageData.temp.toString(), function (err, obj) {
                if (err != null) {
                    console.log(error)
                }
            })
        }
        else{
            temperature = parseFloat(str.replace(',','.'));
        }

		sendRoomTempToClients(temperature,location.roomId);
    });
}
//startMqtt();

var prevTemps=[25,25,25,25,25,25,25,25,25,25,25]

//simulates sensor inputs
var publishTemperature = function(){
    var d = new Date();
    d.setMilliseconds(0)
    d.setSeconds(0)
    d.setMinutes(0)

    for(var i=0; i<=10;i++){
        var tempChange = Math.floor( Math.random() * (1-(-1)+1) + (-1) )
        var newTemp = prevTemps[i] + tempChange
        newTemp = newTemp<20 ? 20 : newTemp
        newTemp = newTemp>30 ? 30 : newTemp
        prevTemps[i]=newTemp

        //client.publish(i+'/Thingy Environment Service/Thingy Temperature Characteristic', JSON.stringify({temp: newTemp, dateTime: d.getTime()}));

		//workaround because Mqtt broker is down
		sendRoomTempToClients(newTemp,i);
		//temperature, buildingId, roomId, dateTime
		storeRoomTempInDB(newTemp, 0,i,d);
    }
}
publishTemperature()

setInterval(function () {
    publishTemperature()
}, 5000);
//}, 1*1000*60*60);

exports.mqttClient=toad;
