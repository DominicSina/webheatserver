module.exports={
    colorIntervals: [
        {min: 0,  max:22, color:{r:0.2,g:0.2,b:0.8,a:0.2}},
        {min: 22, max:25, color:{r:0.2,g:0.5,b:0.5,a:0.2}},
        {min: 25, max:29.5, color:{r:0.2,g:0.8,b:0.2,a:0.2}},
        {min: 29.5, max:35, color:{r:0.5,g:0.5,b:0.2,a:0.2}},
        {min: 35, max:100, color:{r:0.8,g:0.2,b:0.2,a:0.2}}
    ]
}