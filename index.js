const server=require('./config/servers')
require('./routes/socketRoutes')
require('./routes/routes')
require('./config/mqtt')

server.server.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

server.server.listen(server.nodePort, server.bindAddress, () => console.log('Example app listening on port '+server.nodePort));
