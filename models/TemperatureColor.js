const colorIntervals=require('../config/config').colorIntervals

function TemperatureColor(temperature)
{
    this.temperature = temperature;
    this.color = this.temperatureToColor(temperature);
}

TemperatureColor.prototype.temperatureToColor = function(temperature){
    var color={r:0.5,g:0.2,b:0.5,a:0.2}
    colorIntervals.forEach(function(item,index){
        if(temperature>=item.min && temperature<=item.max){
            color = item.color
        }
    })
    return color
}

exports.TemperatureColor = TemperatureColor;

